import 'package:flutter/cupertino.dart';

class ProductItem {
  final int id;
  final String title;
  final int price;
  final String image;
  final String gear;
  final String size;
  final String description;
  final String idCategory;
  final String country;
  final String model;
  final int rate;

  ProductItem({
    @required this.id,
    @required this.title,
    @required this.price,
    @required this.image,
    @required this.gear,
    @required this.size,
    @required this.description,
    @required this.idCategory,
    @required this.country,
    @required this.model,
    @required this.rate,
  });
  ProductItem.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        price = json['price'],
        image = json['image'],
        gear = json['gear'],
        size = json['size'],
        description = json['description'],
        idCategory = json['idCategory'],
        country = json['country'],
        model = json['model'],
        rate = json['rate'];
}
