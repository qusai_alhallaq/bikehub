import 'dart:convert';
import 'package:bikehub/helper/http_exception.dart';
import 'package:bikehub/models/product_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class Products with ChangeNotifier {
  List<ProductItem> _products = [];
  List<ProductItem> get products {
    return [..._products];
  }

  static int lastPage;

  Future<List<ProductItem>> getProductAtPage(int pageId) async {
    final url = 'http://bikehub.store/api/customer/products?page=$pageId';
    final response = await http.get(url);
    if (response.statusCode != 200) {
      throw HttpException(
          'an error occured while get products from server😫.. please try later');
    }
    final responseData = json.decode(response.body);
    lastPage = responseData['data']['last_page'];
    // print(lastPage);
    List<ProductItem> loadedProducts = [];
    for (var product in responseData['data']['data']) {
      loadedProducts.add(ProductItem.fromJson(product));
    }
    print(loadedProducts);
    // if (_products.length > 0) {
    //   // print('test');
    //   _products.addAll(loadedProducts);
    // }
    // _products = loadedProducts;
    // notifyListeners();
    return pageId > lastPage ? [] : loadedProducts;
  }
}
