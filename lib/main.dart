import 'package:bikehub/provider/products.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './screens/products_overview_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Products(),
        ),
      ],
      child: MaterialApp(
        title: 'bikehub',
        theme: ThemeData.dark().copyWith(accentColor: Color(0xFFff6600)),
        home: ProductsOverviewScreen(),
      ),
    );
  }
}