import 'package:bikehub/models/product_item.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:readmore/readmore.dart';

class ProductDetailesScreen extends StatelessWidget {
  final ProductItem product;
  ProductDetailesScreen({@required this.product});
  @override
  Widget build(BuildContext context) {
    TextStyle labelStyle = TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
        color: Theme.of(context).accentColor);
    return Scaffold(
      appBar: AppBar(
        title: Text(product.title),
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: [
          Hero(
            tag: product.id,
            child: CachedNetworkImage(
              imageUrl: 'http://bikehub.store/Images/${product.image}',
              placeholder: (ctx, _) => Image.asset(
                'assets/images/placeholder.png',
                fit: BoxFit.cover,
              ),
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Discription : ',
                style: labelStyle,
              ),
              Expanded(
                child: ReadMoreText(
                  product.description,
                  trimLines: 2,
                  colorClickableText: Theme.of(context).accentColor,
                  trimMode: TrimMode.Line,
                  trimCollapsedText: 'Read more',
                  trimExpandedText: 'Read less',
                  moreStyle: labelStyle,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Text(
                'Price : ',
                style: labelStyle,
              ),
              Text('${product.price} \$')
            ],
          ),
          SizedBox(
            height: 10,
          ),
          product.gear == null
              ? Container()
              : Row(
                  children: [
                    Text(
                      'Gear : ',
                      style: labelStyle,
                    ),
                    Text('${product.gear}')
                  ],
                ),
          SizedBox(
            height: 10,
          ),
          product.model == null
              ? Container()
              : Row(
                  children: [
                    Text(
                      'Model : ',
                      style: labelStyle,
                    ),
                    Text('${product.model}')
                  ],
                ),
          SizedBox(
            height: 10,
          ),
          product.country == null
              ? Container()
              : Row(
                  children: [
                    Text(
                      'Country : ',
                      style: labelStyle,
                    ),
                    Text('${product.country}')
                  ],
                ),
        ],
      ),
    );
  }
}
