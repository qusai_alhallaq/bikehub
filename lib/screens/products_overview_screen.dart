import 'package:bikehub/models/product_item.dart';
import 'package:bikehub/provider/products.dart';
import 'package:bikehub/widgets/product_widget.dart';
import 'package:flutter/material.dart';
import 'package:pagination_view/pagination_view.dart';
import 'package:provider/provider.dart';

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  int page;
  PaginationViewType paginationViewType;
  GlobalKey<PaginationViewState> key;

  Future<List<ProductItem>> getPro(int offset) async {
    page++;
    List<ProductItem> products =
        await Provider.of<Products>(context, listen: false)
            .getProductAtPage(page);
    return products;
  }

  @override
  void initState() {
    paginationViewType = PaginationViewType.listView;
    key = GlobalKey<PaginationViewState>();
    page = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Products'),
      ),
      body: PaginationView<ProductItem>(
        key: key,
        padding: EdgeInsets.all(10),
        paginationViewType: paginationViewType,
        separatorBuilder: (ctx, _) => SizedBox(
          height: 10,
        ),
        itemBuilder: (BuildContext context, product, int index) =>
            ProductWidget(product: product),
        pageFetch: getPro,
        onError: (dynamic error) => Center(
          child: Text(
              'an error occured while get products from server😫 \n please try later'),
        ),
        onEmpty: Center(
          child: Text('Sorry! This is empty'),
        ),
        bottomLoader: Center(
          child: CircularProgressIndicator(),
        ),
        initialLoader: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
