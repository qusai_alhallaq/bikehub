import 'package:bikehub/models/product_item.dart';
import 'package:bikehub/screens/product_detailes_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ProductWidget extends StatelessWidget {
  final ProductItem product;
  ProductWidget({@required this.product});
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    return AspectRatio(
      aspectRatio:
          mediaQueryData.orientation == Orientation.landscape ? 20 / 9 : 16 / 9,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: GridTile(
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (ctx) => ProductDetailesScreen(product: product),
                ),
              );
            },
            child: Hero(
              tag: product.id,
              child: CachedNetworkImage(
                imageUrl: 'http://bikehub.store/Images/${product.image}',
                placeholder: (ctx, _) => Image.asset(
                  'assets/images/placeholder.png',
                  fit: BoxFit.cover,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
          footer: GridTileBar(
            backgroundColor: Colors.black54,
            leading: Row(
              children: [
                Icon(
                  Icons.attach_money,
                  color: Theme.of(context).accentColor,
                ),
                Text(
                  product.price.toString(),
                  style: TextStyle(color: Theme.of(context).accentColor),
                ),
              ],
            ),
            title: Text(
              product.title,
              textAlign: TextAlign.center,
              style: TextStyle(color: Theme.of(context).accentColor),
            ),
            trailing: RatingBarIndicator(
              rating: 2.5,
              // double.parse(product.rate.toString()),
              itemBuilder: (context, index) => Icon(
                Icons.star,
                color: Theme.of(context).accentColor,
              ),
              itemCount: 5,
              itemSize: 20.0,
              direction: Axis.horizontal,
            ),
          ),
        ),
      ),
    );
  }
}
